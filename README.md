# Overview

A demo application that lets you manage patents in a decentralized way
This project uses several concepts (Event Driven Architecture, Domain Driven Design, Microservices and Smart contract)

## Technologies used for continuous integration part 
 * gitlab-ci
 * docker

## Technologies used for blockchain part
 * Solidity
 * Openzeppelin
 * Web3j
 * Ganache-cli
 
## Technologies used for rest service part 
 * Java
 * Sprintboot
 * IPFS
 * Mongodb
 * Swagger
 
## Technologies used for front part 
 * Reactjs
 * Redux
 
## Technologies used for communication between microservices
 * Apache Kafka

# Overview

Business logic is divided into the following services :

![Microservices](doc/bounded-context.jpg)

## Continuous integration

* The [microservices](https://gitlab.com/matkt/patent-filing-microservices) submodule :

 * Gitlab-ci allows to launch the tests automatically on the project ([pipeline](https://gitlab.com/matkt/patent-filing-microservices/pipelines))

* The [smart contract](https://gitlab.com/matkt/patent-filing-contract) submodule :

 * Gitlab-ci allows to launch tests on this project with ethereumjs-testrpc
 * Gitlab-ci allows to create and deploy automatically the documentation of this contract ([pipeline](https://gitlab.com/matkt/patent-filing-contract/pipelines))

* The [front](https://gitlab.com/matkt/patent-filing-front) submodule

## Architecture 

The backend part is divided into two microservices. the first "patent-filing" will manage the patent deposits and the second "event-watcher" will read the events coming from the blockchain

* event-watcher : it will listen to events from ethereum network. As soon as a new patent is added on the blockchain it will be, thanks to this service, link to the database of the server.
* patent-filing : it will allow centralized storage of additional information related to the patent that does not need to be stored in the blockchain (because of storage costs on the ethereum network).

There is also 
* ipfs : Peer-to-peer method of storing and sharing hypermedia in a distributed file system. It will allow to store the PDF which will contain the details of the patent in a decentralized way.
* mongodb : The database of the project.
* kafka :  The broker that will allow the "event-watcher" service to notify the "patent-filing" service that a new patent has just been added to the blockchain. 

This results in the following architecture:

![arch](doc/architecture.jpg)

## Sequence diagram

![sequence-diagram-patent-filing](doc/patent-filing-sequence.png)

# Run the application

All components are launched via dockers

* Start all containers

```
docker-compose -f docker-compose.yml up -d
```

This command will start :

* event-watcher microservice
* patent-filing microservice
* front
* mongodb database
* kafka broker
* IPFS node
* ganache  (personal Ethereum blockchain)
* deploy the contract on the ganache blockchain

![docker ps](doc/dockerps.jpg)

# Documentation

* You can see the swagger documentation of the patent-filing microservice via [http://IP_DOCKER:8090/swagger-ui.html](http://IP_DOCKER:8090/swagger-ui.html)
* You can see the auto generated documentation of the smart contract via [https://matkt.gitlab.io/patent-filing-contract/](https://matkt.gitlab.io/patent-filing-contract/)

![swagger](doc/swagger.jpg)
![contract-doc](doc/contract-doc.jpg)

# Front 

Visit :  http://IP-DOCKER:3000/

The front will allow you to add patents and display the list of patents already filed

## Step 1 : Add a patent

![front step1](doc/front_add_patent.jpg)

## Step 2 : Accept blockchain transaction

![front step2](doc/front_add_patent_2.jpg)

## Step 3 : Check the status of this new patent

![front step3](doc/front_view_patent.jpg)



